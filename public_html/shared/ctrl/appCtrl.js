/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



controllers.controller('AppCtrl', 
    ['$scope','$timeout','ServiceFactory','$interval','$location','AnalyticsFactory','$window','localStorageService',
    function($scope, $timeout, ServiceFactory,$interval,$location,AnalyticsFactory,$window,localStorageService ) {       
    
    $scope.status={label:"Online", cssStyle:'text-success',online:true};
    $scope.styleFix={"min-height":($window.innerHeight-102)+"px"};
    $scope.messageHeight={"min-height":($window.innerHeight-200)+"px"};
    $scope.isManager =false;
    $scope.projectName = "aoc";
    $scope.serverPrefix=location.protocol+"//"+location.hostname+":"+location.port+"/"+$scope.projectName+"/";   
    $scope.common="http://sistema.net.ar/common/";
    $scope.main={force:false,loading:true,accionEnProgreso:"El sistema se esta iniciando.", toolImage:$scope.serverPrefix+"assets/images/waiting/giphy.gif"};
    $scope.status.cssStyle = "text-info";
    $scope.acciones={};
    $scope.customUserList=[];
    $scope.forms={}; 
    $scope.proveedores={};
    $scope.transportistas={list:[],keys:{}};
    $scope.clientes={list:[],keys:{}};
    $scope.assets={provincias:{list:[],keys:{}}};    
    $scope.user={};
    $scope.hiddenAttrs = true;
    $scope.confirmarAccion={show:false, titulo:'Esta seguro?', mensaje:[], onOk:null, onCancel:null };
    $scope.storageKeys={
        clientes:"clientes",
        transportistas:"transportistas",
        nuevoEnvio:"nuevoEnvio1",
        proveedores:"proveedores",
        provincias:"provincias"
    };
    
       
    $scope.opts = {
        maxDate: moment().subtract(20, 'days'),
        locale: {
            format: 'DD/MM/YYYY',
            separator: ' - ',
            applyClass: 'btn-green',
            applyLabel: "Aplicar",
            fromLabel: "Desde",
            toLabel: "Hasta",
            cancelLabel: 'Cerrar',
            customRangeLabel: 'Otro rango',            
            daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            firstDay: 1,
            monthNames: [
                'Enero', 'Febrero', 'Marzo', 'Abril', 
                'mayo', 'Junio', 'Julio', 'Agosto',
                'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
            ]
        },
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 Dias': [moment().subtract(6, 'days'), moment()],
            '30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este mes': [moment().startOf('month'), moment().endOf('month')],
            'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    };    
    $scope.singleOpts = {       
        singleDatePicker: true,
        showDropdowns: true,
        startDate: moment().format("L"),
        endDate: moment().format("L"), 
        autoApply:true,
        autoUpdateInput:true,        
        locale: {            
            format: 'DD/MM/YYYY',
            separator: ' - ',
            applyClass: 'btn-green',
            applyLabel: "Aplicar",
            fromLabel: "Desde",
            toLabel: "Hasta",
            cancelLabel: 'Cerrar',
            customRangeLabel: 'Otro rango',            
            daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            firstDay: 1,
            monthNames: [
                'Enero', 'Febrero', 'Marzo', 'Abril', 
                'mayo', 'Junio', 'Julio', 'Agosto',
                'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
            ]
        }
    };
    
    $scope.broadCastManager=function(p_eventName,p_arguments){
        $timeout(function(){
            $scope.$broadcast(p_eventName, p_arguments);        
        },100 );
    };
   ;
    $scope.colapseLeftSide=function(){
        $.AdminLTE.pushMenu.forceCollapse();
    };
    
     $scope.msgboxManager=function(p_accion, p_titulo, p_mensaje){
        var _config ={show:true, waiting:false, message:p_mensaje,title:p_titulo,cssClass:'modal-success'};
        if (p_accion ==="espere"){            
            _config.waiting = true;            
        }else if(p_accion ==="success" || p_accion ==="ok"){
            _config.cssClass='modal-success';    
        }else if(p_accion ==="danger" || p_accion ==="error"){
            _config.cssClass='modal-danger';
            if (typeof p_mensaje =="undefined"){_config.message = 'Algo salio mal.';}
        }else if(p_accion ==="warning" || p_accion ==="advertencia"){
            _config.cssClass='modal-warning';
        }else{
            _config.show = false;            
            _config.waiting = false;         
            $timeout(function(){
                $(".select2").select2({ width: 'resolve' });
            },1200);
        }
        $scope.messageStatus=_config;                
    };    
    $scope.actionPopup=function(p_titulo, p_mensajeArray, p_onOk, p_onCancel ){
        var _innerOk = function(){};
        var _innerCancel = function(){};
        var _ok={acion:_innerOk,texto:"Sí"};
        var _cancel={acion:_innerCancel,texto:"No"};
        var _confirmarAccion={show:false, titulo:'Esta seguro?', mensaje:[], 
            onOk:_ok, onCancel:_cancel
        };
        if(typeof p_titulo != "undefined" ){
            if (typeof p_mensajeArray != "object"){
                var _array = new Array();
                _array.push(p_mensajeArray);
                p_mensajeArray = _array;
            };
            if (typeof p_onOk == "object"){_ok = p_onOk;}
            if (typeof p_onCancel == "object"){_cancel = p_onCancel;}
            _confirmarAccion={show:true, titulo:p_titulo, mensaje:p_mensajeArray, onOk:_ok, onCancel:_cancel };
        };      
        $scope.confirmarAccion = _confirmarAccion;
    };
    
    var _hiddenWaitting = function(){
        $scope.main.loading = false;
        $scope.main.force = false;
    };
    
    var _init =function(){
        //$.AdminLTE.pushMenu.forceCollapse();
        $timeout( _preInitAdmin, 1000);
        $scope.broadCastManager('INIT_COMPLETE');
    };
    
    //iniciar
    $scope.$on("INIT_COMPLETE",_hiddenWaitting);    
    $timeout(_init,1200);
    
        
    
}]);
