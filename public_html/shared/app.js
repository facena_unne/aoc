

var aocApp = angular.module('aocApp',[
    'controllers','ngRoute',
    'LocalStorageModule','angular-select-text','filters',
    'angularInlineEdit',
    'ui.mask','daterangepicker',
    'ngLocationUpdate',
    'ngTagsInput',
    'chart.js'
]);  

moment.locale("es");

var controllers = angular.module('controllers', []);
var filters = angular.module('filters', []);

aocApp.config(['$routeProvider', 'localStorageServiceProvider',
    function( $routeProvider, localStorageServiceProvider ) {    
    
    localStorageServiceProvider.setPrefix('transporteApp')
    .setStorageType('localStorage')
    .setNotify(true, true);
   
    
    $routeProvider 
        .when('/binario', {
            templateUrl: 'components/binario/binarioView.html',
            controller: 'BinarioCtrl'
        })
        .when('/ca', {
            templateUrl: 'components/ca/caView.html',
            controller: 'CaCtrl'
        })
        .when('/exceso', {
            templateUrl: 'components/exceso/excesoView.html',
            controller: 'ExcesoCtrl'
        })
        .otherwise({
            redirectTo: '/binario'
    });   
}]);
