/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


aocApp.factory('ServiceFactory', ['$http', '$q','$timeout',
    function ($http, $q,$timeout) {
    
    var pub={};
    pub.serverPrefix=location.protocol+"//"+location.hostname+":"+location.port+"/transporte/";
    var _defaultMaxRecall = 5;
    pub.maxRecall =angular.copy(_defaultMaxRecall);
    var _isSaveData=function(response){       
        if (response.data.hasOwnProperty("error")){
            if (response.data.error === 2 || response.data.error === 1){//hibernate exception                
                return false;                
            }else{
                if (response.data.hasOwnProperty('list')){
                    if (response.data.list.length===0){                       
                        return false;                       
                    }else{
                        pub.maxRecall =angular.copy(_defaultMaxRecall);
                        return true;
                    }                    
                }else if (response.data.hasOwnProperty('saved')){
                    if (response.data.saved){pub.maxRecall =_defaultMaxRecall;}
                    return response.data.saved;
                }
                else{//no tiene lista.
                    pub.maxRecall =angular.copy(_defaultMaxRecall);
                    return true;
                }                
            }
        }else{
            pub.maxRecall =angular.copy(_defaultMaxRecall);
            return true;
        }
    };
    var contextTipeList=[ 
        {'Content-Type':'application/json;charset=utf-8'},
        {'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'},
        {'Content-Type':'application/text;charset=UTF-8'}
    ];
    pub.recal=function(p_functionName,p_service, p_data){
        return $timeout(function(){
            if (pub.maxRecall>=0){
                pub.maxRecall--;
                pub[p_functionName](p_service, p_data, false);
            }else{
                pub.maxRecall =angular.copy(_defaultMaxRecall);
                return $q.reject({error:99,descripction:'max call'});                
            }
        },3000);
        
    };
    pub.get= function( p_service, p_data, p_first) {            
        if (p_first){pub.maxRecall =angular.copy(_defaultMaxRecall);}
        // the $http API is based on the deferred/promise APIs exposed by the $q service
        // so it returns a promise for us by default
        var _req = {
            method: "GET",
            url:  p_service.indexOf('http://') !== -1 ? p_service : pub.serverPrefix + p_service ,
            headers: contextTipeList[0], 
            params: p_data         
        };
        return $http(_req)
            .then(function(response) {
                if (typeof response.data === 'object') {
                    if (_isSaveData(response)){
                        return response.data;
                    }else{
                        return  pub.recal('get',p_service, p_data); //$timeout(function(){pub.get(p_service, p_data)},3000);
                    }
                } else {
                    // invalid response
                    return $q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return $q.reject(response.data);
            });
    };
    pub.post= function(p_service, p_data,p_first) {            
        if (p_first){pub.maxRecall =angular.copy(_defaultMaxRecall);}
        // the $http API is based on the deferred/promise APIs exposed by the $q service
        // so it returns a promise for us by default
        var _data = $.param(p_data);
        return $http({method:'POST',
                url: pub.serverPrefix+p_service, 
                data:_data,
                headers: contextTipeList[1]
            })
            .then(function(response) {
                if (typeof response.data === 'object') {
                    if (_isSaveData(response)){
                        return response.data;
                    }else{                        
                        return  pub.recal('post',p_service, p_data); //
                    }
                } else {
                    // invalid response
                    return $q.reject(response.data);
                }
            }, function(response) {
                // something went wrong
                return $q.reject(response.data);
            });
    };
    
     pub.login= function(p_service, p_data,p_first) {            
        if (p_first){pub.maxRecall =angular.copy(_defaultMaxRecall);}
        // the $http API is based on the deferred/promise APIs exposed by the $q service
        // so it returns a promise for us by default
        var _data = $.param(p_data);
        return $http({method:'POST',
                url: pub.serverPrefix+p_service, 
                data:_data,
                headers: contextTipeList[1]
            })
            .then(function(response) {
                if (typeof response.data === 'object') {
                    if (_isSaveData(response)){
                        return response.data;
                    }else{                        
                        return pub.recal('login',p_service, p_data); //
                    }
                } else {
                    // invalid response
                    return $q.reject(response.data);
                }
            }, function(response) {
                // something went wrong
                return $q.reject(response.data);
            });
    };
        
    return pub;
}]);
