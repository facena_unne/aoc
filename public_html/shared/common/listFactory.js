/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



aocApp.factory('ListFactory', [function () {
    
    var pub={};
    
    pub.listKey ='articulos';
    
    var _random = function(){
        return Math.floor(Math.random()*10000);
    };
    
    pub.deployJsonPath=function(){
        return 'services/deploy'+(location.hostname==='localhost'?'_locale':'')+'.json?noCache=' + _random();
    };
    pub.articulosJsonPath=function(manager){
        if (manager){
            return 'services/getArticulos';
        }else{
            return 'services/articulos'+(location.hostname==='localhost'?'_locale':'')+'.json?noCache=' + _random();
        }
    };
    
    return pub;
}]);