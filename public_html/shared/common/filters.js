filters.filter('removeAcentosFilter',function(){    
    var pub =function( p_texto ) {            
        var p_texto =p_texto.replace(new RegExp("[á]", 'g'),"a");
            p_texto =p_texto.replace(new RegExp("[é]", 'g'),"e");
            p_texto =p_texto.replace(new RegExp("[í]", 'g'),"i");
            p_texto =p_texto.replace(new RegExp("[ó]", 'g'),"o");
            p_texto =p_texto.replace(new RegExp("[ú]", 'g'),"u");
        return p_texto;
    };
    return pub;
});


/**
 * @use : filterEstadoLabel 
 * @use : removeAcentosFilter 
 */
filters.filter('comprasFilter',
['$filter',function($filter){    
    var pub =function( p_compras, p_texto, p_staticAssets, p_proveedores ) {            
        var _array =p_compras;
       if ( typeof p_texto != "undefined")
       {          
           if (p_texto !== "")
           {
                p_texto = $filter('removeAcentosFilter')(p_texto).toLowerCase();
                var _array =[];                
                angular.forEach(p_compras,function(_compra){                       
                    var _responsabilidada = p_staticAssets.responsabilidades[p_staticAssets.responsabilidadesKey[p_proveedores.list[p_proveedores.keys[_compra.idProveedor]].idResponsabilidadIVA]].label.toLowerCase();
                    var _nroDoc = p_proveedores.list[p_proveedores.keys[_compra.idProveedor]].nroDocumento;
                    var _alias = p_proveedores.list[p_proveedores.keys[_compra.idProveedor]].alias.toLowerCase();
                    var _razonSocial = p_proveedores.list[p_proveedores.keys[_compra.idProveedor]].razonSocial.toLowerCase();
                    
                    if ( 
                        _responsabilidada.indexOf(p_texto) !==-1
                        || _nroDoc.indexOf(p_texto) !==-1
                        || _razonSocial.indexOf(p_texto) !==-1
                        || _alias.indexOf(p_texto) !==-1
                    )
                    {
                        _array.push(_compra);
                    }                        
                });
                                
                
            }
        }        
        return _array;
    };
    return pub;
}]);

filters.filter('fullDireccionFilter',
['$scope',function($scope){    
    var pub =function( p_direccion ) {            
        var p_texto =p_direccion.direccion + ', ' + 
        $scope.assets.provincias.list[$scope.assets.provincias.keys[p_direccion.idProvincia]].localidades.list[
            $scope.assets.provincias.list[$scope.assets.provincias.keys[p_direccion.idProvincia]].localidades.keys[p_direccion.idLocalidad]
        ].nombre+ ' ' + $scope.assets.provincias.list[$scope.assets.provincias.keys[p_direccion.idProvincia]].nombre;;
        return p_texto;
    };
    return pub;
}]);
filters.filter('noRemovedFilter',function(){    
    var pub =function( p_items ) {            
        var _array =[];
        
        angular.forEach(p_items, function( _item ){
            if (!p_items.hasOwnProperty('removed'))
            {
                _array.push( _item );
            }
        });
        return _array;
    };
    return pub;
});

filters.filter('removeNoIdElementFilter',function(){    
    var pub =function( p_items ) {            
        var _array =[];
        
        angular.forEach(p_items, function( _item ){
            if ( _item.id!==0)
            {
                _array.push( _item );
            }
        });
        return _array;
    };
    return pub;
});

filters.filter('lblDestinatarioDireccion',function(){    
    var pub =function( p_destinatario, p_clientes, p_provincias ) {            
        var _cliente = p_clientes.list[p_clientes.keys[p_destinatario.id]];
        var _direcciones = _cliente.direcciones;
        var _destino = _direcciones.list[_direcciones.keys[p_destinatario.idDireccion]];
        var _localidadAbreviatura = p_provincias.list[p_provincias.keys[_destino.idProvincia]].localidades.list[
            p_provincias.list[p_provincias.keys[_destino.idProvincia]].localidades.keys[_destino.idLocalidad]
        ].abreviatura;
        return  _cliente.nombre + ", " +_destino.direccion + ' ' + (typeof _localidadAbreviatura ===""? 'FALTA Abreviatura!' : _localidadAbreviatura);
    };
    return pub;
});

filters.filter('lblLocalidadDeLaDireccion',function(){    
    var pub =function( p_destinatario, p_clientes, p_provincias ) {            
        var _cliente = p_clientes.list[p_clientes.keys[p_destinatario.id]];
        var _direcciones = _cliente.direcciones;
        var _destino = _direcciones.list[_direcciones.keys[p_destinatario.idDireccion]];
        if (_destino.hasOwnProperty("idProvincia")){
            return p_provincias.list[p_provincias.keys[_destino.idProvincia]].localidades.list[
                p_provincias.list[p_provincias.keys[_destino.idProvincia]].localidades.keys[_destino.idLocalidad]
            ].nombre;        
        }else{
            console.log(_destino);
        }
    };
    return pub;
});

filters.filter('lblDireccionLocalidadProvincia',function(){    
    var pub =function( p_direccion, p_clientes, p_provincias ) {            
        return p_direccion.direccion + ', ' + 
            p_provincias.list[p_provincias.keys[p_direccion.idProvincia]].localidades.list[
                p_provincias.list[p_provincias.keys[p_direccion.idProvincia]].localidades.keys[p_direccion.idLocalidad]
            ].nombre+ ' ' + p_provincias.list[p_provincias.keys[p_direccion.idProvincia]].nombre;   
    };
    return pub;
});
filters.filter('lblDireccion',function(){    
    var pub =function( p_destinatario, p_clientes ) {            
         var _direcciones = p_clientes.list[p_clientes.keys[p_destinatario.id]].direcciones;
        var _destino = _direcciones.list[_direcciones.keys[p_destinatario.idDireccion]];
        if (_destino.hasOwnProperty('direccion')){
            return  _destino.direccion;
        }else{
            console.log(p_destinatario);
            return "";
        }
        
    };
    return pub;
});

filters.filter('clientesActivos',function(){    
    var pub =function( p_clientes ) {            
        var _array =[];        
        angular.forEach(p_clientes, function( _cliente ){
            if( _cliente.idEstado===1 )
            {
                _array.push( _cliente );
            }
        });
        return _array;
    };
    return pub;
});




