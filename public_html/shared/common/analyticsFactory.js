(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-96547042-1', 'auto');
ga('set', 'hostname', window.location.protocol + '//' + window.location.hostname +window.location.pathname + window.location.search);
        
//TODO : cambiar ua
aocApp.factory('AnalyticsFactory', [function () {
        
    var pub = {};
    pub.lastestPage = "landing";
    
    pub.init=function(callback){
        pub.sendPageView(pub.lastestPage); //landing        
        if (typeof callback !== "undefined")callback();
    };
    pub.sendPageView=function(p_page){	
        pub.setPage(p_page);
        ga('send', {
            'hitType': 'pageview',
            'page': p_page,
            'title': 'aki'
        });          
    };
    pub.setPage=function(p_page){ 
      pub.lastestPage = p_page;       
      ga('set', {
        'hitType': 'pageview',
        'page': p_page,
        'title': 'transporte'
      });
    };
    /**     
     * @param {type} info = {category:'' action:'', label, value:0}     
     */
    pub.sendEvent=function(info){      
        if(!info.hasOwnProperty("value")){info.value= 0;}         
        if(!info.hasOwnProperty("action")){info.action= "";}  
        if(!info.hasOwnProperty("label")){info.label= "";}              
       				
        ga('send', 'event', {
                  'eventCategory': info.category,
                  'eventAction': info.action,
                  'eventLabel':info.label,
                  'eventValue':info.value
                });
    };
    
    return pub;
    
}]);