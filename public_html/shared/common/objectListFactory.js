/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


aocApp.factory('ObjectListFactory', ['$timeout',
    function ($timeout) {    
    var pub={};
    /**
     * Actualiza la lista, en caso de que la clave ya exista actualiza la informacion
     * en el caso de que el objeto no este, lo agrega, llamando a add
     * @param {Object} p_elementToAdd 
     * @param {object} p_object {list:[], keys:{}}     
     * @see {function} pub add
     */
    pub.update=function( p_elementToAdd, p_object ){
        if (p_object.keys.hasOwnProperty(p_elementToAdd.id)){
            if (p_elementToAdd.id ===0){
                pub.add( p_elementToAdd, p_object );
            }else{
                p_object.list[p_object.keys[p_elementToAdd.id]] = p_elementToAdd;    
            }
        }
        else
        {
            pub.add( p_elementToAdd, p_object );
        }        
    };
    
    /**
     * Agrega un objeto al master object y actualiza las keys  
     * @param {type} p_elementToAdd objecto para agregar al master object
     * @param {type} p_object master object = { list:[], keys:[] }     
     */
    pub.add=function( p_elementToAdd, p_object ){
        p_object.list.push(p_elementToAdd);
        p_object.keys={};
        pub.resetKeys(p_object);        
    };
    pub.remove=function( p_element, p_object ){
        p_object.list.splice(p_object.keys[p_element.id],1);
        pub.resetKeys(p_object);
    };
    pub.resetKeys=function(p_object){
        p_object.keys={};
        angular.forEach(p_object.list,function( _element, p_index ){
            p_object.keys[_element.id] = p_index;
        });
    };
    
    
    return pub;
}]);

