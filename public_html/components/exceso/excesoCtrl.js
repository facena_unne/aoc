/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


controllers.controller('ExcesoCtrl',
    ['$scope', '$location','AnalyticsFactory',
    function ($scope, $location,AnalyticsFactory) {

    $scope.inEdit= {number:0, word:8, maximo:127, minimo:-128};
    
    $scope.resultados = {exceso:0,binario:0, array:[] };
    
    $scope.setWordSize=function(p_size){
        if (typeof p_size == "undefiner"){
            p_size = $scope.inEdit.word;
        }   
        $scope.inEdit.word = p_size; 
        $scope.inEdit.maximo= Math.pow(2,p_size-1)-1;
        $scope.inEdit.minimo= -1*Math.pow(2,p_size-1);
        $scope.convert();    
    };
    
    $scope.addMinus=function(){
        $scope.inEdit.number=$scope.inEdit.number*-1;
        $scope.convert();    
    };
    
    $scope.convert=function(){
        $scope.resultados.error= "";       
        if ( (typeof  $scope.inEdit.number === "number") && Math.floor($scope.inEdit.number) === $scope.inEdit.number ){
            
            if ($scope.inEdit.number>0){
                $scope.resultados.exceso =  $scope.inEdit.maximo +$scope.inEdit.number;
            }else{
                $scope.resultados.exceso = $scope.inEdit.number + ($scope.inEdit.minimo*-1);
            }
            $scope.resultados.binario = ($scope.resultados.exceso >>> 0).toString(2);
            $scope.resultados.array = $scope.resultados.binario.split("");
            
            if ( $scope.resultados.array.length <= $scope.inEdit.word){
                for(var i = $scope.resultados.array.length; i < $scope.inEdit.word; i++){
                    $scope.resultados.array.unshift("0");
                }
            }else{
                $scope.resultados.error= $scope.inEdit.word+" bits no son sufienciente para representar este numero";
            } 
            
            
        }else{
            $scope.resultados.error= "Algo salio mal. :_(";
        }
        AnalyticsFactory.sendEvent({category:'conversion',action:'decimal-exceso',label:$scope.inEdit.number});                        
    };
    

    
    var _init = function(){
        AnalyticsFactory.sendPageView("exceso");
    };
    
    _init();
}]);


