/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


controllers.controller('CaCtrl',
    ['$scope', '$location','AnalyticsFactory',
    function ($scope, $location,AnalyticsFactory) {

    $scope.inEdit= {number:0, word:8};
    $scope.resultados = {num:0, array:[], complemento:[], error:"", ca2:[],ca2Num:0};
    
    $scope.setWordSize=function(p_size){
        if (typeof p_size == "undefiner"){
            p_size = $scope.inEdit.word;
        }   
        $scope.inEdit.word =p_size;    
        $scope.convert();    
    };
    $scope.addMinus=function(){
        $scope.inEdit.number=$scope.inEdit.number*-1;
        $scope.convert();    
    };
    $scope.convert=function(){
      
            if ( (typeof  $scope.inEdit.number === "number") && Math.floor($scope.inEdit.number) === $scope.inEdit.number ){
                
                $scope.resultados.error= "";       
                $scope.resultados.ca2=[];
                $scope.resultados.num = (Math.abs($scope.inEdit.number) >>> 0).toString(2);                        
                $scope.resultados.array = $scope.resultados.num.split("");                

                if ( $scope.resultados.array.length < $scope.inEdit.word){
                    for(var i = $scope.resultados.array.length; i < $scope.inEdit.word; i++){
                        $scope.resultados.array.unshift("0");
                    }
                }else{
                    $scope.resultados.error= $scope.inEdit.word+" bits no son sufienciente para representar este numero en Modulo y signo.";
                } 
                
                if ( $scope.inEdit.number < 0 ){
                    //complementamos                    
                    for(var i = 0; i < $scope.resultados.array.length; i++){
                        if ($scope.resultados.array[i] == "0"){
                            $scope.resultados.array[i]="1";    
                        }else{
                            $scope.resultados.array[i]="0";
                        }
                    }
                    
                    var _acarreo=1;
                    var _ca2 = angular.copy($scope.resultados.array);
                    //sumamos uno, despreciamos e                    
                    for(var j = $scope.resultados.array.length-1; j >=0; j--){
                        if (_acarreo > 0 ){
                            if ($scope.resultados.array[j] == "0"){
                                _ca2[j]="1";    
                                _acarreo=0;
                            }else{
                                _ca2[j]="0";
                                _acarreo =1;
                            }
                        }
                    }
                    
                    $scope.resultados.ca2Num =_ca2.toString().replace( new RegExp(",",'g'),"") ;              
                    $scope.resultados.ca2 =String($scope.resultados.ca2Num).split("");
                    
                    AnalyticsFactory.sendEvent({category:'conversion',action:'decimal-ca2',label:$scope.inEdit.number});                        
                }else{
                    AnalyticsFactory.sendEvent({category:'conversion',action:'decimal-ca1',label:$scope.inEdit.number});                        
                }
                
                
                //_sistema.bitsCount = _sistema.rta.length;
                
            }            
       
       
       
    };
    

    
    var _init = function(){
        AnalyticsFactory.sendPageView("ca");
    };
    
    _init();
}]);


