/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


controllers.controller('BinarioCtrl',
    ['$scope', '$location','AnalyticsFactory',
    function ($scope, $location, AnalyticsFactory) {

    $scope.inEdit= {number:0};
    $scope.sistemas={
        binario:{base:2,rta:0,bitsCount:0}, 
        octal:{base:8,rta:0,bitsCount:0},
        hexa:{base:16,rta:0,bitsCount:0},
        
    };
    
    
    $scope.dec2bin=function(){
        angular.forEach($scope.sistemas,function(_sistema, _nombre){
            
            //var _foo = parseInt($scope.inEdit.number, 10);
            if ( (typeof  $scope.inEdit.number === "number") && Math.floor($scope.inEdit.number) === $scope.inEdit.number ){
                _sistema.rta = ($scope.inEdit.number >>> 0).toString(_sistema.base);    
                _sistema.bitsCount = _sistema.rta.length;
               
            }else{
                var _floor = Math.floor($scope.inEdit.number);
                var _binaryFloor = (_floor >>> 0).toString(_sistema.base);             
                var _noIntegerBinario =  ($scope.inEdit.number % 1).toString(_sistema.base);            

                if (_sistema.base>10){
                    _sistema.rta = _binaryFloor + "," + String(_noIntegerBinario).substring(2,6);
                }else{
                    _sistema.rta =  _binaryFloor + "," + String(_noIntegerBinario).substring(2,6);
                }
                _sistema.bitsCount = _sistema.rta.length-1;
                
            }
             AnalyticsFactory.sendEvent({category:'conversion',action:'decimal-binario',label:$scope.inEdit.number});                        
            
        });
        
       
       
    };
    
    var _init = function(){
        AnalyticsFactory.sendPageView("binario");
    };
    
    _init();

}]);


